#!/usr/bin/env python

"""
Open relevant page in Bitbucket.

This command works for both Git and Mercurial project.

Examples
--------

Open top page in Bitbucket::

    %(prog)s

Open blame/annotation page of a given file::

    %(prog)s annotate FILE

View source code in bitbucket::

    %(prog)s src FILE

View issues page::

    %(prog)s issue

Create a new issue::

    %(prog)s new_issue


License
-------

Copyright (c) 2013 Takafumi Arakaki

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

"""

# the original idea is from here:
#   http://hgtip.com/tips/advanced/2009-10-08-open-bitbucket-from-bash/

import os


### Repository finder

def find_root_directory(metadir, path):
    """
    Find a parent directory of `path` containing `metadir`.

    :type metadir: str
    :arg  metadir: '.git' or '.hg'
    :type    path: str
    :arg     path: Current working directory.

    """
    if path.endswith(os.path.sep):
        path = os.path.dirname(path)
    path = os.path.abspath(path)
    old = None
    while path and path != old:
        if os.path.isdir(os.path.join(path, metadir)):
            return path
        old = path
        path = os.path.dirname(path)


def find_vcs_root(path):
    """
    Find a VCS repository.

    :rtype: 2-tuple or None
    :return: A pair of root path of the VCS repository and its type
             ('git' or 'hg').

    """
    candidates = list(filter(
        lambda x: x[0],
        ((find_root_directory(md, path), md[1:]) for md in ('.git', '.hg'))))
    if candidates:
        return sorted(candidates, key=lambda x: len(x[0]), reverse=True)[0]


### Repository configuration parser

def get_config(path=None):
    if not path:
        path = os.getcwdu()
    vcs = find_vcs_root(path)
    if not vcs:
        raise RuntimeError("'{0}' not in Git or HG repository.".format(path))
    getter = get_config_git if vcs[1] == 'git' else get_config_hg
    return getter(path, vcs[0])


def get_config_git(path, repopath):
    import subprocess

    output = subprocess.check_output(['git', 'remote', '-v'])
    for (remote, url, _) in parse_git_remote_v(output):
        if remote == 'origin':
            break
    else:
        raise RuntimeError('No remote named origin')

    abspath = os.path.abspath(path)
    relpath = os.path.relpath(abspath, repopath)
    return dict(
        type='git',
        rev='master',
        origpath=path,
        repopath=repopath,
        relpath=relpath,
        abspath=abspath,
        path=url,
        url=path_to_bitbucket_url(url),
    )


def parse_git_remote_v(output):
    r"""
    Parse `output` of the command "git remote -v".

    >>> list(parse_git_remote_v('''\
    ... origin\tssh://git@bitbucket.org/USER/REPOSITORY.git (fetch)
    ... origin\tssh://git@bitbucket.org/USER/REPOSITORY.git (push)\
    ... '''))                               #doctest: +NORMALIZE_WHITESPACE
    [('origin', 'ssh://git@bitbucket.org/USER/REPOSITORY.git', 'fetch'),
     ('origin', 'ssh://git@bitbucket.org/USER/REPOSITORY.git', 'push')]

    """
    for line in output.splitlines():
        (remote, rest) = line.split('\t', 1)
        (url, annotation) = rest.split(' ', 1)
        yield (remote, url, annotation.strip('()'))


def get_config_hg(path, repopath):
    from mercurial import hg
    from mercurial import ui
    repo = hg.repository(ui.ui(), path=repopath)
    if not repo:
        return
    default_path = repo.ui.config('paths', 'default')
    abspath = os.path.abspath(path)
    relpath = os.path.relpath(abspath, repopath)
    return dict(
        type='hg',
        rev='tip',
        origpath=path,
        repopath=repopath,
        relpath=relpath,
        abspath=abspath,
        path=default_path,
        url=path_to_bitbucket_url(default_path),
        )


def path_to_bitbucket_url(path):
    import re
    match = re.match(r'.*://(\w*@)?(bitbucket.org.*?)(\.git)?$', path)
    if not match:
        return
    url = 'https://{0}'.format(match.group(2))
    if not url.endswith('/'):
        url += '/'
    return url


### Main

def open_bitbucket(goto, file):
    config = get_config(file)
    page = pages.get(aliases.get(goto, goto))
    import webbrowser
    if isinstance(page, str):
        url = config['url'] + page
    else:
        url = page(config)
    print "Open:", url
    webbrowser.open_new_tab(url)


### URL dispatchers

def open_src(config):
    if config['origpath']:
        path = '/'.join(['src', config['rev'], config['relpath']])
    else:
        path = 'src'
    return config['url'] + path


def open_annotate(config):
    if config['origpath'] and os.path.isfile(config['abspath']):
        path = '/'.join(['annotate', config['rev'], config['relpath']])
    else:
        path = 'src'
    return config['url'] + path


def make_aliases(pages):
    aliases = {}
    for p in pages:
        aliases[p[0]] = p
    # just override the ambiguous pages
    aliases['a'] = 'annotate'
    aliases['m'] = 'admin'
    return aliases


def make_choices(pages, aliases):
    return ["{0}({1})".format(p, aliases_back[p]) for p in sorted(pages)]


pages = dict(
    download='download',
    src=open_src,
    changesets='changesets',
    wiki='wiki',
    issue='issues?status=new&status=open',
    new_issue='issues/new',
    admin='admin',
    annotate=open_annotate,
    )
aliases = make_aliases(pages)
aliases_back = dict((v, k) for (k, v) in aliases.iteritems())
pages[None] = ''


### CLI

def choices():
    for p in pages:
        if isinstance(p, str):
            yield p
            yield aliases_back[p]


def main():
    import argparse

    class Formatter(argparse.RawTextHelpFormatter,
                    argparse.RawDescriptionHelpFormatter,
                    argparse.ArgumentDefaultsHelpFormatter):
        pass

    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=Formatter,
    )
    parser.add_argument(
        'goto', choices=list(choices()), nargs='?',
        help="page to go")
    parser.add_argument(
        'file', nargs='?',
        help="relevant file")
    args = parser.parse_args()

    open_bitbucket(**vars(args))


if __name__ == '__main__':
    main()
